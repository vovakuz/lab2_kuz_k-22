﻿#include <iostream>
#include <string>
#include <utility>

using namespace std;


int base = 10;

class LongNumbers {

public:
    
    string value;
    
    LongNumbers(string x) {
        value = move(x);
    }

    LongNumbers() {
        
        value = "0";
    }
    
    static long leveler(string& num1, string& num2) {
        
        long n = max(num1.size(), num2.size());

        while (num1.size() < n)
            num1.insert(0, "0");

        while (num2.size() < n)
            num2.insert(0, "0");

        return n;
    }
    
    static string add(string num1, string num2) {
        
        int spare = 0;
        long n = leveler(num1, num2);
        string res;
        int sum;

        for (int i = n - 1; i >= 0; i--) {
            sum = (num1[i] - '0') + (num2[i] - '0') + spare;
            res.insert(0, to_string(sum % base));
            spare = sum / base;
        }

        if (spare) {
            res.insert(0, to_string(spare));
        }

        return res.erase(0, min(res.find_first_not_of('0'), res.size() - 1));
    }
   
    static string sub(string num1, string num2) {
        
        int diff;
        long n = leveler(num1, num2);
        string x, y, res;

        if (num1 > num2) {
            x = num1;
            y = num2;
        }
        else {
            x = num2;
            y = num1;
        }

        for (int i = n - 1; i >= 0; i--) {
            diff = (x[i] - '0') - (y[i] - '0');

            if (diff >= 0)
                res.insert(0, to_string(diff));

            else {
                int prev = i - 1;
                while (prev >= 0) {
                    x[prev] = (base + (x[prev] - '0') - 1) % base + '0';

                    if (x[prev] != '9')
                        break;
                    else
                        prev--;
                }
                res.insert(0, to_string(diff + base));
            }
        }
        return res.erase(0, min(res.find_first_not_of('0'), res.size() - 1));
    }

    static string x10_multiplier(string& num, long times) {
        
        for (int k = 0; k < times; k++)
            num.append("0");

        return num;
    }

    LongNumbers& operator=(string x) {
        
        *this = LongNumbers(move(x));

        return *this;
    }
    
    LongNumbers operator+(const LongNumbers& x) {

        return LongNumbers(add((*this).value, x.value));
    }

    LongNumbers operator-(const LongNumbers& x) {

        return LongNumbers(sub((*this).value, x.value));
    }

    static string Karatsuba(LongNumbers num1, LongNumbers num2){

        LongNumbers res, a0, a1, b0, b1, m1, m2, c1, c2, c1_c2, Z1;
        long n = LongNumbers::leveler(num1.value, num2.value);

        if (n == 1) {
            return to_string((num1.value[0] - '0') * (num2.value[0] - '0'));
        }

        a0 = num1.value.substr(0, n / 2);
        a1 = num1.value.substr(n / 2, n - n / 2);
        b0 = num2.value.substr(0, n / 2);
        b1 = num2.value.substr(n / 2, n - n / 2);

        m1 = Karatsuba(a0, b0);
        m2 = Karatsuba(a1, b1);
        c1 = (a0 + a1);
        c2 = (b0 + b1);
        c1_c2 = Karatsuba(c1, c2);
        Z1 = c1_c2 - m1 - m2;

        LongNumbers::x10_multiplier(m1.value, 2 * (n - n / 2));
        LongNumbers::x10_multiplier(Z1.value, (n - n / 2));

        res = m1 + m2 + Z1;

        return res.value.erase(0, min(res.value.find_first_not_of('0'), res.value.size() - 1));
    }

    static string Toom_Cook(LongNumbers num1, LongNumbers num2){

        LongNumbers res, a0, a1, a2, b0, b1, b2, m1, m2, m0, c1, c2, c3, m01, m02, m12;
        long l = LongNumbers::leveler(num1.value, num2.value);

        if (l == 1) {
            return to_string((num1.value[0] - '0') * (num2.value[0] - '0'));
        }

        if (l % 3) {
            l += 3 - l % 3;
            while (num1.value.size() < l)
                num1.value.insert(0, "0");
            LongNumbers::leveler(num1.value, num2.value);
        }

        a0 = num1.value.substr(0, l / 3);
        a1 = num1.value.substr(l / 3, l / 3);
        a2 = num1.value.substr(2 * l / 3, l / 3);
        b0 = num2.value.substr(0, l / 3);
        b1 = num2.value.substr(l / 3, l / 3);
        b2 = num2.value.substr(2 * l / 3, l / 3);

        m0 = Toom_Cook(a0, b0);
        m1 = Toom_Cook(a1, b1);
        m2 = Toom_Cook(a2, b2);

        c1 = Toom_Cook((a0 + a1), (b0 + b1));
        c2 = Toom_Cook((a0 + a2), (b0 + b2));
        c3 = Toom_Cook((a1 + a2), (b1 + b2));
        
        m01 = c1 - m0 - m1;
        m02 = c2 - m0 - m2;
        m12 = c3 - m1 - m2;

        m0 = LongNumbers::x10_multiplier(m0.value, 4 * l / 3);
        m01 = LongNumbers::x10_multiplier(m01.value, 3 * l / 3);
        m1 = LongNumbers::x10_multiplier(m1.value, 2 * l / 3);
        m02 = LongNumbers::x10_multiplier(m02.value, 2 * l / 3);
        m12 = LongNumbers::x10_multiplier(m12.value, l / 3);

        res = m0 + m1 + m2 + m01 + m02 + m12;

        return res.value.erase(0, min(res.value.find_first_not_of('0'), res.value.size() - 1));
    }
};

long long power(long long a, long long m, long long mod) {
    
    long long res = 1;
    a = a % mod;

    while (m > 0) {
        if (m % 2)
            res = (res * a) % mod;

        a = (a * a) % mod;
        m /= 2;
    }
    return res % mod;
}

bool Lemer(long long n, long long k) {
    
    if (n != 2 && !(n % 2)) {
        return false;
    }
    for (int i = 0; i < k; i++)
    {
        long long a = rand() % (n - 2 + 1) + 2;
        long long p = (n - 1) / 2;
        int r = power(a, p, n);

        if (r != 1 && r != n - 1) {
            return false;
        }
        if (r == 1 || r == (n - 1) % n) {
            return true;
        }
    }
}

bool Rabin_M(long long n, long long k) {

    if (n != 2 && !(n % 2)) {
        return false;
    }

    long long m = n - 1;

    while (!(m % 2)) {
        m /= 2;
    }

    for (int i = 0; i < k; i++)
    {
        long long a = rand() % n;
        long long u = power(a, m, n);

        while (m != n - 1 && u != 1 && u != n - 1) {
            u = power(u, 2, n);
            m *= 2;
        }
        if (u != n - 1 && !(m % 2)) {
            return false;
        }
    }
    return true;
}

int jacobi(long long n, long long k) {

    n %= k;
    int t = 1;

    while (n != 0) {
        while (n % 2 == 0) {
            n /= 2;
            int r = k % 8;

            if (r == 3 || r == 5) {
                t = -t;
            }
        }
        swap(n, k);
        if (n % 4 == 3 && k % 4 == 3) {
            t = -t;
        }
        n %= k;
    }
    if (k == 1)
    {
        return t;
    }
    else
    {
        return 0;
    }
}

bool Solovey(long long n, long long k) {
    int j;
    long long a, p, r;
    for (int i = 0; i < k; ++i) {
        a = rand() % (n - 2 + 1) + 2;
        p = (n - 1) / 2;
        r = power(a, p, n);
        
        if (r != 1 && r != n - 1) {
            return false;
        }
        
        j = jacobi(a, n);
        
        if (r != j % n) {
            return false;
        }
        return true;
    }
}

int main() {

    LongNumbers a, b;
    a = "13212414124";
    b = "2343124124124124124";
    
    long long x, y;
    x = 100003;
    y = 4;

    int choise = -1;
    while (choise)
    {
        cout << "Enter number of the method:";
        cin >> choise;
        switch (choise)
        {

        case 1:
            cout << LongNumbers::Karatsuba(a, b) << endl;
            break;
        case 2:
            cout << LongNumbers::Toom_Cook(a, b) << endl;
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;
        case 6:
            break;
        case 7:
            cout << Lemer(x, y) << endl;
            break;
        case 8:
            cout << Rabin_M(x, y) << endl;
            break;
        case 9:
            cout << Solovey(x, y) << endl;
            break;
        case 10:
            break;
        }
    }
}